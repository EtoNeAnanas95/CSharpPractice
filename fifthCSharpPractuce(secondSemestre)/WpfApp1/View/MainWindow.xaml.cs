﻿using System.Windows;
using WpfApp1.ViewModel;

namespace WpfApp1.View;

/// <summary>
///     Interaction logic for MainWindow.xaml
/// </summary>
public partial class MainWindow : Window
{
    private readonly MainViewModel _mainViewModel;

    public MainWindow()
    {
        InitializeComponent();
        _mainViewModel = new MainViewModel();
        _mainViewModel.SwitchPages += (sender, args) => SwitchPages();
        MainFrame.Content = new WhatWeatherPage(_mainViewModel);
        DataContext = _mainViewModel;
    }

    private void SwitchPages()
    {
        if (MainFrame.Content is WeatherCityPage) MainFrame.Content = new WhatWeatherPage(_mainViewModel);
        else MainFrame.Content = new WeatherCityPage(_mainViewModel);
    }
}