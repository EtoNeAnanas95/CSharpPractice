﻿using System.Windows.Controls;
using WpfApp1.ViewModel;

namespace WpfApp1.View;

public partial class WhatWeatherPage : Page
{
    public WhatWeatherPage(MainViewModel mainViewModel)
    {
        InitializeComponent();
        DataContext = mainViewModel;
    }
}