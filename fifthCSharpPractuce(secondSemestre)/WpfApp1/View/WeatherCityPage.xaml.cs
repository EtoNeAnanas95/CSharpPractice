﻿using System.Windows.Controls;
using WpfApp1.ViewModel;

namespace WpfApp1.View;

public partial class WeatherCityPage : Page
{
    public WeatherCityPage(MainViewModel mainViewModel)
    {
        InitializeComponent();
        foreach (var item in mainViewModel.Forecasts)
        {
            WeatherCard children = new WeatherCard();
            children.MaxTemp = item.Main.TemperatureMax.ToString();
            children.MinTemp = item.Main.TemperatureMin.ToString();
            children.Description = item.Weather[0].Description;
            MainStackPanel.Children.Add(children);
        }
        DataContext = mainViewModel;
    }
}