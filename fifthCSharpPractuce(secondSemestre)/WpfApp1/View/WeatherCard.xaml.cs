﻿using System.Windows.Controls;

namespace WpfApp1.View;

/// <summary>
///     Логика взаимодействия для WeatherCard.xaml
/// </summary>
public partial class WeatherCard : UserControl
{
    public WeatherCard()
    {
        InitializeComponent();
        DataContext = this;
    }

    public string MaxTemp { get; set; }
    public string MinTemp { get; set; }
    public string Description { get; set; }
}