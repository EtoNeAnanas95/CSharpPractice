﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using Weather.NET;
using Weather.NET.Enums;
using Weather.NET.Models.WeatherModel;
using WpfApp1.ViewModel.Helper;

namespace WpfApp1.ViewModel;

public class MainViewModel : BindingHelper
{
    public List<WeatherModel> Forecasts { get; set; }

    private string _currentTemp;

    public string CurrentTemp
    {
        get => _currentTemp;
        set
        {
            _currentTemp = "Current temp\n" + value;
            OnPropertyChanged();
        }
    }
    
    private string _city;

    public string City
    {
        set
        {
            _city = value;
            OnPropertyChanged();
        }
        get => _city;
    }

    public event EventHandler SwitchPages;

    public async void Submit_textbox(object sender, KeyEventArgs e)
    {
        if (e.Key == Key.Enter)
        {
            var currentSender = (TextBox)sender;

            var client = new WeatherClient("d44e7d14b7773ad984fbd46c6a60cf1e");
            try
            {
                Forecasts = await client.GetForecastAsync(currentSender.Text, 8, Measurement.Metric, Language.Russian);
                City = currentSender.Text;
                CurrentTemp = Forecasts[0].Main.Temperature.ToString();
                BackToWhatWeatherPage();
            }
            catch (Exception exception)
            {
                MessageBox.Show("City isn't exist!!!");
            }
        }
    }

    public void Disable_hint(object sender, EventArgs e)
    {
        var currentSender = (TextBox)sender;
        if (currentSender.Text == "Type city") currentSender.Text = string.Empty;
    }
    
    public void BackToWhatWeatherPage()
    {
        SwitchPages?.Invoke(this, EventArgs.Empty);
    }
    
    
}